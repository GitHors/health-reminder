package com.healthreminder.denis.healthreminder;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.healthreminder.denis.healthreminder.actions.UserActions;
import com.healthreminder.denis.healthreminder.database.ConnectionProvider;
import com.healthreminder.denis.healthreminder.models.User;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class LoginActivity extends AppCompatActivity {

    ImageView back;
    Button signInButton;
    EditText login, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        back = (ImageView)findViewById(R.id.back);
        signInButton = (Button)findViewById(R.id.signin);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(LoginActivity.this, StartActivity.class);
                startActivity(it);
            }
        });

        // Input fields for login and password
        login = (EditText) findViewById(R.id.login);
        password = (EditText) findViewById(R.id.password);

    }


    public void loginClickTreatment(View view) {
        String userLogin = login.getText().toString();
        String userPassword = password.getText().toString();

        new ConnectionAsync(userLogin, userPassword).execute();
    }

    private class ConnectionAsync extends AsyncTask<Void, Void, Void> {

        private String login;
        private String password;

        public ConnectionAsync(String login, String password) {
            this.login = login;
            this.password = password;
        }

        boolean res;
        @Override
        protected Void doInBackground(Void... params) {
            Connection connection = null;
            try {
                connection = ConnectionProvider.getConnection();
                res = new UserActions().isUserInfoCorrect(connection, new User(login, password));
            } finally {
                try {
                    if (connection != null){
                        connection.close();
                    }
                } catch (final Exception e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
        if (res) {
            Intent it = new Intent(LoginActivity.this, MainScreenActivity.class);
            startActivity(it);
        } else {
            Toast toast = Toast.makeText(getApplicationContext(),
                    "Не верный логин или пароль", Toast.LENGTH_SHORT);
            toast.show();
        }
            super.onPostExecute(aVoid);
        }
    }
}
