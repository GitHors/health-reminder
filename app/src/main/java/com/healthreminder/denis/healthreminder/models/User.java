package com.healthreminder.denis.healthreminder.models;

public class User {

    int id;
    String name;
    String login;
    String password;
    Role role;

    public User() {
    }

    /**
     *
     * @param id
     * @param login
     * @param password
     * @param role
     */
    public User(int id, String login, String password, Role role) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.role = role;
    }

    /**
     *
     * @param id
     * @param login
     * @param password
     * @param roleName
     */
    public User(int id, String login, String password, String roleName) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.role = new Role(roleName);
    }

    /**
     *
     * @param login
     * @param password
     */
    public User(String username, String login, String password) {
        this.name = username;
        this.login = login;
        this.password = password;
//        this.role = new Role(roleName);
    }

    public User(String name, String login, String password, String roleName) {
        this.name = name;
        this.login = login;
        this.password = password;
        this.role = new Role(roleName);
    }

    /**
     *
     * @param login
     * @param password
     */
    public User(String login, String password) {
        this.login = login;
        this.password = password;
    }

        public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public String getLogin() {
        return login;
    }

    /**
     *
     * @param login
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     *
     * @return
     */
    public String getPassword() {
        return password;
    }

    /**
     *
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     *
     * @return
     */
    public Role getRole() {
        return role;
    }

    /**
     *
     * @param role
     */
    public void setRole(Role role) {
        this.role = role;
    }
}
