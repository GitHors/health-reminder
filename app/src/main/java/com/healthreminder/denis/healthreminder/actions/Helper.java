package com.healthreminder.denis.healthreminder.actions;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Helper {

    public Helper() {

    }

    public static String setParameters(int amount){
        String result = " ";
        for (int i = 0; i < amount - 1; i++){
            result += "?, ";
        }
        result += "?";
        return  result;
    }

    /**
     * Method used for generate date in format '2016-11-25'.
     *
     * @return String
     */
    public static String getDateForDB() {
        final DateFormat df = new SimpleDateFormat("yyyy-dd-MM");
        final Date today = Calendar.getInstance().getTime();
        return df.format(today);
    }
}
