package com.healthreminder.denis.healthreminder.actions;

import com.healthreminder.denis.healthreminder.database.ConnectionProvider;
import com.healthreminder.denis.healthreminder.models.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;


public class UserActions {

    public UserActions() {
    }

    /**
     * Method for check state of user login.
     *
     * @param login
     *            String value.
     * @return boolean value (true - if user with such name already exist).
     */
    public boolean isLoginExist(final String login) {

        boolean result = false;
        Connection connection = null;
        try {
            connection = ConnectionProvider.getConnection();
            final PreparedStatement ps = connection.prepareStatement("SELECT login FROM User WHERE login = ?");

            ps.setString(1, login);
            final ResultSet resultSet = ps.executeQuery();
            result = resultSet.next();
        } catch (final Exception e) {

        }
        return result;
    }

    public boolean isUserInfoCorrect(Connection connection, User user){
        try {
            final PreparedStatement preparedStatement = connection.prepareStatement(
                    "SELECT * FROM User  WHERE login = ? AND password = ?");

            preparedStatement.setString(1, user.getLogin());
            preparedStatement.setString(2, user.getPassword());

            final ResultSet resultSet = preparedStatement.executeQuery();
            return resultSet.next();
        } catch (final Exception e) {
            System.out.println(e.getMessage());
        }
        return false;
    }

    public int addNewUser(Connection connection, User user) {
        int status = 0;
        try {
            connection = ConnectionProvider.getConnection();

            final PreparedStatement preparedStatement = connection.prepareStatement(
                    "INSERT INTO User (name, login, password, id_role) VALUES (?, ?, ?, 1)"
            );
            preparedStatement.setString(1, user.getName());
            preparedStatement.setString(2, user.getLogin());
            preparedStatement.setString(3, user.getPassword());

            status = preparedStatement.executeUpdate();
        } catch (final Exception e) {
            System.out.println(e.getMessage());
        }

        return status;
    }

}
