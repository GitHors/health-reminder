package com.healthreminder.denis.healthreminder.database;

import java.sql.Connection;
import java.sql.DriverManager;

public class ConnectionProvider {

    private static final String URL = "jdbc:mysql://sql11.freemysqlhosting.net/sql11174403?characterEncoding=UTF-8";
    private static final String USER = "sql11174403";
    private static final String PASSWORD = "y9fTcZjhbH";

    /**
     * Constructor.
     */
    public ConnectionProvider() {

    }

    /**
     * Method for create connection with DB.
     *
     * @return
     */
    public static Connection getConnection() {
        Connection connection = null;

        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(URL, USER, PASSWORD);
        } catch (final Exception e) {
            System.out.print(e.getMessage());
        }

        return connection;
    }

}
