package com.healthreminder.denis.healthreminder.models;

public class Role {

    String roleName;
    String roleInfo;

    /**
     *
     */
    public Role() {
    }

    /**
     *
     * @param roleName
     * @param roleInfo
     */
    public Role(String roleName, String roleInfo) {
        this.roleName = roleName;
        this.roleInfo = roleInfo;
    }

    /**
     *
     * @param roleName
     */
    public Role(String roleName) {
        this.roleName = roleName;
    }

    /**
     *
     * @return
     */
    public String getRoleName() {
        return roleName;
    }

    /**
     *
     * @param roleName
     */
    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    /**
     *
     * @return
     */
    public String getRoleInfo() {
        return roleInfo;
    }

    /**
     *
     * @param roleInfo
     */
    public void setRoleInfo(String roleInfo) {
        this.roleInfo = roleInfo;
    }
}
