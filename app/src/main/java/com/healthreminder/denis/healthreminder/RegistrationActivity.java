package com.healthreminder.denis.healthreminder;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.healthreminder.denis.healthreminder.actions.UserActions;
import com.healthreminder.denis.healthreminder.database.ConnectionProvider;
import com.healthreminder.denis.healthreminder.models.User;

import java.sql.Connection;

public class RegistrationActivity extends AppCompatActivity {

    EditText username, login, password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        username = (EditText) findViewById(R.id.username);
        login = (EditText) findViewById(R.id.login);
        password = (EditText) findViewById(R.id.password);
    }

    public void registrationClickTreatment(View view) {
            String userNameValue = username.getText().toString();
            String userLogin = login.getText().toString();
            String userPassword = password.getText().toString();

        new ConnectionAsync(userNameValue, userLogin, userPassword).execute();
    }

    private class ConnectionAsync extends AsyncTask<Void, Void, Void> {

        private String username;
        private String login;
        private String password;

        public ConnectionAsync(String userNameValue, String login, String password) {
            this.username = userNameValue;
            this.login = login;
            this.password = password;
        }

        int res;
        @Override
        protected Void doInBackground(Void... params) {
            Connection connection = null;
            try {
                connection = ConnectionProvider.getConnection();
                res = new UserActions().addNewUser(connection, new User(username, login, password));
            } finally {
                try {
                    if (connection != null){
                        connection.close();
                    }
                } catch (final Exception e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (res != 0) {
                Intent it = new Intent(RegistrationActivity.this, LoginActivity.class);
                startActivity(it);
                Toast toast = Toast.makeText(getApplicationContext(),
                        "Вы успешно зарегистрированы", Toast.LENGTH_SHORT);
                toast.show();
            } else {
                Toast toast = Toast.makeText(getApplicationContext(),
                        "Ошибка", Toast.LENGTH_SHORT);
                toast.show();
            }
            super.onPostExecute(aVoid);
        }
    }
}
